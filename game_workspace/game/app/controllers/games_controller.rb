class GamesController < ApplicationController

  def index
    @tmp_arr = ["Kayu", "Jamu", "Susu" , "Roti"]

    if params[:game].present?
      @check = params[:game][:tebak] == params[:game][:key].downcase
      if @check
        flash[:notice] = "Tebakanmu Benar"
      else
        flash[:notice] = "Tebakanmu Salah"
      end
    end

  end

end
